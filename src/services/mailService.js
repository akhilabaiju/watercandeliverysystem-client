import axios from 'axios';
const API_URL = 'http://localhost:3001/api/mail';

const mailService =  {
sendMail:async(id)=>{
    try{
    
      const response =await axios.post(`${API_URL}/sendMail`,id);
      return response
 }catch(error){
   console.error('Error fetching user:', error);
   throw error;
 }
}
}
export default mailService