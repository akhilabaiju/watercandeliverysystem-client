import axios from 'axios';
const API_URL = 'http://localhost:3001/api/user'; 
const userService =  {
    getUserById:async(id)=>{
        try{
             const response =await fetch(`${API_URL}/byid/${id}`);
            return response
            }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getAllUsers:async()=>{
        try{
            const response =await fetch(`${API_URL}/getAllUsers`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getVendor:async()=>{
        try{ 
            const response =await fetch(`${API_URL}/getVendors`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getManufacturer:async(id)=>{
        try{
          console.log("in service")
          const response =await fetch(`${API_URL}/user/manufacturer/${id}`);
          return response
     }catch(error){
       console.error('Error fetching user:', error);
       throw error;
     }
   },


   sendMail:async(id)=>{
    try{
      console.log("in service")
      const response =await axios.post(`${API_URL}/sendMail`,id);
      return response
 }catch(error){
   console.error('Error fetching user:', error);
   throw error;
 }
},

updateUser:async(values)=>{
  try{
    const response =await axios.put(`${API_URL}/update`,values);
   return response;
}catch(error){
  console.error('Error fetching user:', error);
  throw error;
}
},

addAddress:async(values)=>{
  try{
    const response =await axios.post(`${API_URL}/addresses`,values);
   return response;
}catch(error){
  console.error('Error fetching user:', error);
  throw error;
}
},

getAddress:async(id)=>{
  try{
    const response =await fetch(`${API_URL}/addresses/${id}`);
    return response
}catch(error){
 console.error('Error fetching user:', error);
 throw error;
}
},

getAddressByid:async(id)=>{
  try{
   // console.log("in service"+id)
    const response =await fetch(`${API_URL}/addresses/addressbyid/${id}`);
    return response
}catch(error){
 console.error('Error fetching user:', error);
 throw error;
}
},

removeAddress:async(id)=>{
  try{
    const response = axios.put(`${API_URL}/removeaddress/${id}`);
    return response;
  }catch(error){
    console.error('Error fetching user:', error);
    throw error;
  }
}
}
export default userService