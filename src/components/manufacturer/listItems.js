import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

import DashboardIcon from '@mui/icons-material/Dashboard';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';

import AddCircleIcon from '@mui/icons-material/AddCircle';
//import Link from '@mui/material/Link';
import { Link } from "react-router-dom";
export const mainListItems = (
    
  <React.Fragment>
    <ListItemButton component={Link} to="/manufacturer/listproducts">
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItemButton>
    <ListItemButton component={Link} to="/manufacturer/addproducts">
    {/* <ListItem button component={Link} to="/design"> */}
      <ListItemIcon>
        <AddCircleIcon />
      </ListItemIcon>
       <ListItemText primary="Add Products" />
    </ListItemButton>
    <ListItemButton component={Link} to="/manufacturer/orders">
      <ListItemIcon>
        <ShoppingCartIcon />
      </ListItemIcon>
      <ListItemText primary="Orders" />
    </ListItemButton>
   
    
  </React.Fragment>
);

