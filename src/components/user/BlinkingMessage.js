import React from 'react';
import { Button, styled } from '@mui/material';
import { Outlet, useNavigate } from 'react-router-dom'; // Import useNavigate
// Define the keyframes for the blinking animation
const blinkAnimation = styled('div')({
  animation: 'blink 1s infinite',
  '@keyframes blink': {
    '0%': { opacity: 1 },
    '50%': { opacity: 0 },
    '100%': { opacity: 1 },
  },
});

const BlinkingButton = styled(Button)({
  backgroundColor: 'lightblue', // Set your desired background color
});

const BlinkingText = styled('div')({
  textAlign: 'center',
});
const BlinkingMessage = () => {
   const navigate = useNavigate(); // Use useNavigate hook
   const handleVendor = () => {
     navigate('/user/userheader/shopproducts'); // Use navigate to redirect to '/vendor/shopproducts'
  };
  return (
    <BlinkingText>
      <BlinkingButton component={blinkAnimation} fullWidth onClick={handleVendor}>
        You Can Select Vendor Near You for Fast Delivery Here
      </BlinkingButton>
    </BlinkingText>
  );
};

export default BlinkingMessage;
