
import React, { useState, useEffect } from 'react';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import Paper from '@mui/material/Paper';
import orderService from '../../services/orderService';
import productService from '../../services/productService';
import userService from '../../services/userService';
import manufacturerService from '../../services/manufacturerService'
// Generate Order Data
function createData(id, date, name, shipTo, quantity, amount) {
  return { id, date, name, shipTo, quantity, amount };
}

// Helper function to parse and format the date
function formatCreatedAtDate(createdAt) {
  const date = new Date(createdAt);
  return date.toLocaleDateString(); // Adjust the formatting as needed
}

export default function Userorders() {
  const id = localStorage.getItem("id");
  const [orders, setOrders] = useState([]);
  const [users, setUsers] = useState({});
  const [products, setProducts] = useState({});
  const [manufacturers, setManufacturers] = useState({});
  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = async () => {
    try {
      const response = await orderService.getOrders(id);
      if (!response.ok) {
        throw new Error('Failed to fetch orders');
      }
      const data = await response.json();
      setOrders(data);
      const productResponse = await productService.getAllProducts();
            if (!productResponse.ok) {
              throw new Error('Failed to fetch manufacturers');
            }
            const productsData = await productResponse.json();
                    const productsMap = {};
                    productsData.forEach(product => {
                      productsMap[product._id] = {
                        name: product.productName,
                        weight: product.productWeight,
                        price:product.productPrice
                      };
                    });
                    setProducts(productsMap);
      // *****************
            const userResponse = await userService.getAllUsers();
            if (!userResponse.ok) {
              throw new Error('Failed to fetch manufacturers');
            }
            const userData = await userResponse.json();
             const userMap = {};
             userData.forEach(users => {
               userMap[users._id] = users.firstName;
             });
            setUsers(userMap);
             // Fetch manufacturers
        const manufacturerResponse = await manufacturerService.getAllManufacturers();
        if (!manufacturerResponse.ok) {
          throw new Error('Failed to fetch manufacturers');
        }
        const manufacturerData = await manufacturerResponse.json();
        const manufacturerMap = {};
        manufacturerData.forEach(manufacturer => {
          manufacturerMap[manufacturer._id] = manufacturer.firstName + " " + manufacturer.lastName;
        });
        setManufacturers(manufacturerMap);


    } catch (error) {
      console.error(error.message);
    }
  };

  return (
    // <Grid item xs={12}>
      <Paper sx={{ p: 3, display: 'flex', flexDirection: 'column'}}>
        <React.Fragment>
          Recent Orders
          <Table size="small">
            <TableHead>
              <TableRow>
                 <TableCell>Product Name</TableCell>
                 <TableCell>Product Weight(in Liters)</TableCell>
                 <TableCell>Manufacture Name</TableCell>
                 <TableCell>Vendor Name</TableCell>
                 <TableCell>Product Price(Rs.)</TableCell>
                 <TableCell>Quantity</TableCell>
                 <TableCell>Amount Paid</TableCell>
                 <TableCell>Date Of Order</TableCell>
                {/* <TableCell>Delivery Date</TableCell> */}


              </TableRow>
            </TableHead>
            <TableBody>
  {orders && orders.map((row) => (
    <TableRow key={row.id}>
      <TableCell>{products[row.productId]?.name}</TableCell>
      <TableCell>{products[row.productId]?.weight}</TableCell>
      <TableCell>{manufacturers[row.manufactureId]}</TableCell>
      {/* Conditional rendering based on the value of row.vendorId */}
      {row.vendorId === "No Vendor" ? (
        // Render "No Vendor" if vendorId is "No Vendor"
        <React.Fragment>
          <TableCell>{row.vendorId}</TableCell>
          
        </React.Fragment>
      ) : (
        // Render the vendor name if vendorId is not "No Vendor"
        <TableCell>{users[row.vendorId]}</TableCell>
      )}
      <TableCell>{products[row.productId]?.price}</TableCell>
      <TableCell>{row.quantity}</TableCell>
      <TableCell>{`Rs.${row.amount}`}</TableCell>
      <TableCell>{formatCreatedAtDate(row.createdAt)}</TableCell>
      {/* Uncomment this line if needed */}
      {/* <TableCell>{users[row.userId]}</TableCell> */}
    </TableRow>
  ))}
</TableBody>

          </Table>
                  </React.Fragment>
      </Paper>
    // </Grid>
  );
}
