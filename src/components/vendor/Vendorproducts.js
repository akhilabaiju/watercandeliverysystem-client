import React from 'react'
import  { useState, useEffect } from 'react';
import productService from '../../services/productService';
import { Grid, Box } from '@mui/material';
import { Card, CardMedia, CardContent, CardActions, Typography, Button } from '@mui/material';
import cartService from '../../services/cartService';
import CartPage from '../cart/CartPage';


const Vendorproducts = () => {
  const [data, setData] = useState(null);
  const [cartItems, setCartItems] = useState([]);
  const userId= localStorage.getItem("id")
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await productService.getAllProducts();
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        const result = await response.json();

        setData(result);
        const res = await cartService.getCart(userId);
        if (!res.ok) {
              throw new Error('Failed to fetch data');
            }
            const carts = await res.json();
            setCartItems(carts);
      } catch (error) {
        console.error('Error fetching data:', error.message);
      }

    };
    fetchData();
  }, [userId]);

  const getItemQuantityInCart = async (productId) => {
    try {
      const response = await cartService.getItemQuantityInCart(userId, productId);
      if (!response.ok) {
        throw new Error('Failed to fetch quantity');
      }
      const data = await response.json();
      return data.quantity;
    } catch (error) {
      console.error('Error getting item quantity in cart:', error);
      return 0; // Return 0 if there's an error
    }
  };

  const addCart = async(e,product) => {
    e.preventDefault();
    try {
     const productId=product._id;
     const quantityInCart = await getItemQuantityInCart(productId);
     if (quantityInCart === 0) {
          cartService.addToCart({
          manufactureId: product.manufactureId,
          userId: userId,
          productId: product._id,
          quantity: 1,
          productName: product.productName,
          productWeight: product.productWeight,
          productPrice: product.productPrice,
          imageUrl: product.imageUrl
        });
          }
     else{ 
         cartService.updateCart({
          manufactureId: product.manufactureId,
          userId: userId,
          productId: product._id,
          quantity: quantityInCart+1,
      })
    }
    const response = await cartService.getCart(userId);
    if (!response.ok) {
              throw new Error('Failed to fetch data');
            }
            const result = await response.json();
            setCartItems(result);
    
    } catch (error) {
      console.error('Error submitting form:', error);
    }
  }

  const deleteItem = async(item) => {
      await cartService.removeItem(item._id);
      const response = await cartService.getCart(userId);
     if (!response.ok) {
               throw new Error('Failed to fetch data');
             }
             const result = await response.json();
             setCartItems(result);
  };

  const handleQuantityChange = async(event, item) => {
    try {
      // Get the new quantity from the event target value
      const newQuantity = event.target.value;
      // Update quantity in the database
      await cartService.updateCart({
        manufactureId: item.manufactureId,
        userId: userId,
        productId: item.productId,
        quantity: newQuantity
      });
      const response = await cartService.getCart(userId);
      if (!response.ok) {
                throw new Error('Failed to fetch data');
              }
              const result = await response.json();
              setCartItems(result);
    } catch (error) {
      console.error('Error updating quantity:', error);
    }
  };
  

  return (

    // <div><ProductListingPage/></div>
    <Box sx={{ flexGrow: 1, p: 2 }}>
       <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          <Grid container spacing={2}>
            {(data) && data.map((product) => (
              <Grid item key={product._id} xs={12} sm={6} md={4}>
                <Card sx={{ maxWidth: 245 }}>
                  <CardMedia
                    component="img"
                    height="240"
                    image={'http://localhost:3001//' + product.imageUrl.replace(/src\\public\\/, '')}
                    alt={product.name}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {product.productName}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {/* {product.pNumber>=1 }
                      Number of Available Item: {product.pNumber} */}
                        {product.pNumber >= 1 ? `Number of Available Items: ${product.pNumber}` : 'Currently out of stock'}
                    </Typography>
                  </CardContent>
                  <CardActions>
                  {product.pNumber >= 1 ? <Button size="small" onClick={(e) => addCart(e, product)}>
                      Add to Cart
                  </Button> : <Button size="small" disabled>
                      Add to Cart
                  </Button> }
                 
                  </CardActions>
                </Card>

              </Grid>
            ))}
          </Grid>
        </Grid>
        <Grid item xs={12} md={4}>
        
        <CartPage
       cartItems={cartItems}
      handleQuantityChange={handleQuantityChange}
      deleteItem={deleteItem}
    />
        </Grid>
      </Grid>
    </Box>
  )
}


export default Vendorproducts

