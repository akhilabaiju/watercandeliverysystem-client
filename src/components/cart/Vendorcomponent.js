import React, { useState, useEffect } from 'react';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import userService from '../../services/userService';
import NativeSelect from '@mui/material/NativeSelect';
import orderService from '../../services/orderService';
import {Grid,Typography} from "@mui/material";
import cartService from '../../services/cartService';
const Vendorcomponent = () => {
  const [vendors, setVendors] = useState(null);
  const [selectedVendor, setSelectedVendor] = useState(''); // Initialize with an empty string
  const [data, setData] = useState(null);
  const [cartItems, setCartItems] = useState([]);
  const [stockNotAvailable, setStockNotAvailable] = useState(false);

  const userId = localStorage.getItem("id");
  useEffect(() => {
    fetchVendors();
  }, []);
  
  const fetchVendors = async () => {
    try {
      const response = await userService.getVendor();
      if (!response.ok) {
        throw new Error('Failed to fetch vendors');
      }
      const data = await response.json(); 
      setVendors(data);
      
    } catch (error) {
      console.error(error.message);
    }
  };
  
//   const handleVendorChange = async (event) => {
//     const selectedVendorId = event.target.value; 
//     setSelectedVendor(selectedVendorId); 
//     const response = await orderService.getOrders(selectedVendorId);
//         if (!response.ok) {
//           throw new Error('Failed to fetch data');
//         }
//         const result = await response.json();
//         setData(result);

//         const res =await cartService.getCart(userId);
//         if (!res.ok) {
//            throw new Error('Failed to fetch data');
//          }
//          const result1 = await res.json();
//          setCartItems(result1);
//   };
  
const handleVendorChange = async (event) => {
    const selectedVendorId = event.target.value; 
    setSelectedVendor(selectedVendorId); 
  
    try {
      // Fetch data for the selected vendor
      const response = await orderService.getOrders(selectedVendorId);
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const result = await response.json();
      setData(result);
  
      // Fetch cart items for the user
      const res = await cartService.getCart(userId);
      if (!res.ok) {
        throw new Error('Failed to fetch cart items');
      }
      const result1 = await res.json();
      setCartItems(result1);
  
      // Check if cart items are present in the data
      const cartItemIds = cartItems.map(item => item._id);
      const dataItemIds = data.map(item => item.productId);
      if (!cartItemIds.every(id => dataItemIds.includes(id))) {
        console.log('Some cart items are not available in the data');
        // Set a state variable to indicate that stock is not available
        setStockNotAvailable(true);
    } else {
        // Set the state variable to indicate that stock is available
        setStockNotAvailable(false);
    }
    } catch (error) {
      console.error(error.message);
    }
  };
  

  return (
    <div>
        <Grid item xs={12} >
 <Grid item xs={7}>Choose
        <FormControl variant="standard"  sx={{ m: 1, minWidth: 180 }} fullWidth>
                <InputLabel variant="standard" htmlFor="Products">
                   Select Vendor
                </InputLabel>
                <NativeSelect   defaultValue={"--Select--"} onChange={handleVendorChange}>
                <option value={""} ></option>
                {vendors && vendors.map(row => (
                  <option value={row._id} >{row.firstName +" "+row.lastName}</option>
                ))}
                   
                </NativeSelect>
            </FormControl>
       </Grid>
       <Grid item xs={5}>{stockNotAvailable && (
    <Typography variant="body2" color="error">
        Stock not available for some items.
    </Typography>
)}
</Grid>
       </Grid>
           
    </div>
  );
};

export default Vendorcomponent;
